package app.agendaline.di.modules

import app.agendaline.network.AuthService
import app.agendaline.ui.auth.ui.login.data.LoginRemoteDataSource
import app.agendaline.ui.auth.ui.login.data.LoginRepository
import app.agendaline.ui.auth.ui.register.data.RegisterRemoteDataSource
import app.agendaline.ui.auth.ui.register.data.RegisterRepository
import app.agendaline.util.BASEURL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideHTTPClient() = OkHttpClient.Builder().build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpCient: OkHttpClient): Retrofit =
        Retrofit.Builder().baseUrl(BASEURL).addConverterFactory(MoshiConverterFactory.create())
            .client(okHttpCient)
            .build()

    @Provides
    @Singleton
    fun provideAuthAPIService(retrofit: Retrofit): AuthService =
        retrofit.create(AuthService::class.java)

    @Provides
    @Singleton
    fun providesLoginRemoteDataSource(api: AuthService) = LoginRemoteDataSource(api)

    @Provides
    fun providesLoginRepository(remoteDataSource: LoginRemoteDataSource) =
        LoginRepository(remoteDataSource)

    @Provides
    @Singleton
    fun providesRegisterRemoteDataSource(api: AuthService) = RegisterRemoteDataSource(api)

    @Provides
    fun providesRegisterRepository(remoteDataSource: RegisterRemoteDataSource) =
        RegisterRepository(remoteDataSource)
}
