package app.agendaline.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING

}