package app.agendaline.ui.auth.ui.register.ui.login

/**
 * User details post authentication that is exposed to the UI
 */
data class RegisterInUserView(
    val displayName: String
    //... other data fields that may be accessible to the UI
)