package app.agendaline.ui.auth.ui.register.data

import app.agendaline.network.AuthService
import app.agendaline.ui.auth.ui.register.data.model.LoggedInUser
import java.io.IOException
import javax.inject.Inject


class RegisterRemoteDataSource @Inject constructor(val api: AuthService) {

    fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            // TODO: handle loggedInUser authentication
            val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe")
            return Result.Success(fakeUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}