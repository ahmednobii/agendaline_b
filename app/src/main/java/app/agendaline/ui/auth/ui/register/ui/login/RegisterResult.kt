package app.agendaline.ui.auth.ui.register.ui.login

/**
 * Authentication result : success (user details) or error message.
 */
data class RegisterResult(
    val success: RegisterInUserView? = null,
    val error: Int? = null
)