package app.agendaline.ui.main.statue

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import app.agendaline.R

class StatueFragment : Fragment() {

    companion object {
        fun newInstance() = StatueFragment()
    }

    private lateinit var viewModel: StatueViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.statue_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(StatueViewModel::class.java)
        // TODO: Use the ViewModel
    }

}