package app.agendaline.ui.main.util

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import app.agendaline.R
import app.agendaline.ui.main.chat.ChatFragment
import app.agendaline.ui.main.groups.GroupsFragment
import app.agendaline.ui.main.statue.StatueFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

private val fragments: MutableList<Fragment> = arrayListOf(
    ChatFragment(), StatueFragment(), GroupsFragment()
)
private var titles: MutableList<String> = arrayListOf("Chat", "Statue", "Groups")


class SectionsPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    //
//    fun addFragment(fragment: Fragment, title: String) {
//        takeIf { !fragments.contains(fragment) }.apply {
//
//            fragments.add(fragment)
//            titles.add(title)
//        }
//    }
    fun getTitle(position: Int) = titles[position]
//
//    override fun getItem(position: Int): Fragment {
//        return fragments[position]
//
//    }

    //    override fun getCount(): Int = fragments.size
    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment {

        return fragments[position]

    }
}