package app.agendaline.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.agendaline.R
import app.agendaline.ui.main.util.SectionsPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.conversation_fragment, container, false)
    }

    private fun initViewPager() {

        val sectionsPagerAdapter =
            SectionsPagerAdapter(
                this
            )
        Log.d("MainFragment", " size ${sectionsPagerAdapter.itemCount}")
        view_pager.adapter = sectionsPagerAdapter
        TabLayoutMediator(tabs, view_pager) { tab, position ->
            tab.text = sectionsPagerAdapter.getTitle(position)

        }.attach()


    }


}